package DicPanel;

public class Word {
    private String wordTarget, wordExplain;

    public String getWordTarget() { return wordTarget; }

    public void setWordTarget(String wordTarget) { this.wordTarget = wordTarget; }
	
    public String getWordEplain() { return wordExplain;}

    public void setWordEplain(String wordEplain) { this.wordExplain = wordEplain; }
	
    public Word(String wordTarget, String wordExplain) {
	this.wordExplain = wordExplain;
	this.wordTarget = wordTarget;
    }
	
    public String showWord() {
	String src = "";
	src = this.wordTarget + "\t\t" + this.wordExplain;
	return src;
    }        
} 
