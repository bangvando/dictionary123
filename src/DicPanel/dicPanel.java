/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DicPanel;


import java.awt.Color;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.JList;
import javax.swing.DefaultListModel;


public class dicPanel extends JPanel{    
    private JLabel nhap, nghia;
    private JTextField tuTK, nghiaTK;

    private JButton tim, them, xoa, list, dich;
    private JList<String> listSearch, listE;
    private DictionaryCommandLine dLine;
    private ActionListener actionListener;
    private JScrollPane  scrollSearch;
    
    public dicPanel() {
        initPanel();
        addComps();
        addEvents();
    }
    
    private void addEvents() {
        
        actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String a = tuTK.getText();
                String firstLetterA =   a.substring(0, 1);
                String remainingLettersA = a.substring(1, a.length());
                firstLetterA = firstLetterA.toUpperCase();
                a = firstLetterA + remainingLettersA;
                String b = tuTK.getText();
                String firstLetterB =   b.substring(0, 1);
                String remainingLettersB = b.substring(1, b.length());
                firstLetterB = firstLetterB.toUpperCase();
                b = firstLetterB + remainingLettersB;
                String bb = listSearch.getSelectedValue();
                switch(e.getActionCommand()) {
                    case "Search":              
                        listSearch.setModel(dLine.dictionarySearcher(a));
                        break;
                    case "List":
                        listSearch.setModel(dLine.d.dic.listWordE);

                        break;
                    case "Add":
                        if(a == null || b == null) break;
                        else {
                            dLine.d.insertWord(a, b);
                            tuTK.setText("");
                            nghiaTK.setText("");
                            listSearch.setModel(dLine.d.dic.listWordE);
                        }
                        break;
                    case "Remove":
                        dLine.d.removeWord(bb);
                        listSearch.setModel(dLine.d.dic.listWordE);
                        break;
                    case "Translate":
                        
                        tuTK.setText(listSearch.getSelectedValue());
                        nghiaTK.setText(dLine.Translate(bb));                                                                                                                       
                        break;
                    default:
                        break;
                }
            }
        };
        
        tim.addActionListener(actionListener);
        them.addActionListener(actionListener);
        xoa.addActionListener(actionListener);
        dich.addActionListener(actionListener);
        list.addActionListener(actionListener);
        
       
    }
    
    private void addComps() {
        nhap = new JLabel();
        nhap.setText("nhập từ");
        nhap.setBounds(20, 10, 100, 50);
        nhap.setForeground(Color.red);
        add(nhap);
        
        nghia = new JLabel();
        nghia.setText("nghĩa của từ");
        nghia.setBounds(20, 50, 100, 50);
        nghia.setForeground(Color.red);
        add(nghia);
        
        tuTK = new JTextField();
        tuTK.setBounds( 130,20 , 200, 30);
        tuTK.setForeground(Color.red);    
        tuTK.setBackground(Color.BLUE);
        add(tuTK);  
        
        nghiaTK = new JTextField();
        nghiaTK.setBounds( 130,60 , 200, 30);
        nghiaTK.setForeground(Color.red);
        nghiaTK.setBackground(Color.BLUE);
        add(nghiaTK);             
  
        them = new JButton();
        them.setText("Add");
        them.setBounds(150, 100, 80, 30);
        them.setActionCommand("Add");
        add(them);

        Scroll();

        xoa = new JButton();
        xoa.setText("Remove");
        xoa.setBounds(270, 100, 80, 30);
        xoa.setActionCommand("Remove");
        add(xoa);


        tim = new JButton();
        tim.setText("Search");
        tim.setBounds(20, 100, 80, 30);
        tim.setActionCommand("Search");
        add(tim);
        
        dich = new JButton();
        dich.setText("Translate");
        dich.setBounds(80, 140, 100, 30);
        add(dich);
        
        list = new JButton();
        list.setText("List");
        list.setBounds(220, 140, 80, 30);
        list.setActionCommand("List");
        add(list);
        
    }
    private void initPanel() {
        
        dLine = new DictionaryCommandLine();
        setBackground(Color.WHITE);
        setLayout(null);
    }
    
    
    public void Scroll() {
        listSearch = new JList<>();
        DefaultListModel aza = new DefaultListModel();
        listSearch.setModel(aza);
        listSearch.setLayoutOrientation(JList.VERTICAL);
        listSearch.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        scrollSearch = new JScrollPane(listSearch);
        scrollSearch.setBounds(20, 180, 310, 220);
        add(scrollSearch);
    }
    
}
