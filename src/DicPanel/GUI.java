/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DicPanel;
import java.awt.CardLayout;
import javax.swing.JFrame;
/**
 *
 * @author DO VAN BANG
 */
public class GUI extends JFrame{
    private static final int WIDTH_FRAME = 370;
    private static final int HEIGHT_FRAME = 450;
    private dicPanel dictionPanel;
    
    public GUI() {
       
        initGUI();
        addEvents();
        addComps();
    }
    
    private void initGUI() {
        setTitle("DICTIONARY");
        setSize(WIDTH_FRAME, HEIGHT_FRAME);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setResizable(false);
        setLayout(new CardLayout());
    }
    
    private void addEvents() {
        
    }
    
    private void addComps() {
        dictionPanel = new dicPanel();
        add(dictionPanel);
    }
    
    public static void main(String[] args) {
        GUI gui = new GUI();
        gui.setVisible(true);
    }
}
