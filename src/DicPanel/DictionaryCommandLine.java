package DicPanel;

import javax.swing.DefaultListModel;

public class DictionaryCommandLine {
    public DictionaryManagement d = new DictionaryManagement();
    
    
    
    public void showAllWord() {
	System.out.println("No\t  English\t  Vietnamese");
        for(int k = 0; k < d.dic.Words.getSize(); k++) {				
            System.out.println((k+1) + "\t " + d.dic.Words.get(k).showWord());
	}
    }
    
    public DictionaryCommandLine() {
        d.insertFromFile();
        d.dictionaryExportToFile();
    }
    
    public void dictionaryLookup(String tu) {

	for(int k = 0; k < d.dic.Words.getSize(); k++) {
            if(tu.equals(d.dic.Words.get(k).getWordTarget())) {
					
		System.out.println(d.dic.Words.get(k).showWord());
            }
	}
    }
		
    public DefaultListModel dictionarySearcher(String tk) {
        DefaultListModel<String> de = new DefaultListModel();
	for(int k = 0; k < d.dic.Words.getSize(); k++) {
                        if(d.dic.Words.get(k).getWordTarget().contains(tk)) {
		if(tk.charAt(0) == d.dic.Words.get(k).getWordTarget().charAt(0))
                    de.addElement(d.dic.Words.get(k).getWordTarget());
            }
	}
        return de;
    }
    
    public String Translate(String ad) {
        String a = "";
        for(int k = 0; k < d.dic.Words.getSize(); k++) {
            
            if(d.dic.Words.get(k).getWordTarget().contains(ad)) {
                a =  d.dic.Words.get(k).getWordEplain();
            }
        }
        return a;
    }
}    
