package DicPanel;


import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.BufferedWriter;

public class DictionaryManagement {
    public Dictionary dic = new Dictionary();
    public void insertFromFile() {
        try {
            FileReader file = new FileReader("Untitled.txt");
            BufferedReader input = new BufferedReader(file);
            String line = "";
            while(true) {
                line = input.readLine();
                if(line == null) {
                    break;
                }			
                String txt[] = line.split("	",2);
                dic.listWordE.addElement(txt[0]);
                Word a = new Word(txt[0], txt[1]);
                dic.Words.addElement(a);
            }
            file.close();
            input.close();
        } catch(Exception e) {}
    }		
    
    public void insertWord(String wordTarget, String wordExplain) {
        Word b = new Word(wordTarget, wordExplain);
        dic.Words.addElement(b);
        dic.listWordE.addElement(wordTarget);
        
    }
    
    public void removeWord(String wordTarget) {
        for(int i = 0; i < dic.Words.getSize(); i++) {
            if(wordTarget.equals(dic.Words.get(i).getWordTarget())) {                
                dic.Words.removeElementAt(i);
                dic.listWordE.removeElement(wordTarget);
            }
        }
    }
	
    public void dictionaryExportToFile() {
        try {
            FileWriter fileW = new FileWriter("Dictionary.txt");
            BufferedWriter inputW = new BufferedWriter(fileW);
            for(int i = 0; i < dic.Words.getSize(); i++) {
                inputW.write(dic.Words.get(i).getWordTarget() + "	" + dic.Words.get(i).getWordEplain() + "\n");
            }
            inputW.close();
        } catch (Exception e) {}
    }	
}
